import socket
from threading import Thread
import os

clients = []

class ClientListener(Thread):
    def __init__(self, name: str, sock: socket.socket):
        super().__init__(daemon=True)
        self.sock = sock
        self.name = name

    def _close(self):
        clients.remove(self.sock)
        self.sock.close()
        print(self.name + ' disconnected')

    def run(self):
        # rcv filename & modify it if needed
        filename = self.sock.recv(1024).decode('utf-8')
        print('received filename', filename)
        i = 1
        filename_wo_ext = filename.split('.')[0]
        ext = filename.split('.')[1]

        while filename in os.listdir('.'):
            filename = filename_wo_ext + '(' + str(i) + ').' + ext
            i += 1
        print('saved as:', filename)
        self.sock.send('ack'.encode('utf-8'))
        # rcv file
        with open(filename, "wb") as fw:
            byte = self.sock.recv(1024)
            while byte:
                fw.write(byte)
                byte = self.sock.recv(1024)
        self._close()
        return


def main():
    next_name = 1
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('', 8756))
    sock.listen()
    while True:
        con, addr = sock.accept()
        clients.append(con)
        name = 'c' + str(next_name)
        next_name += 1
        print(str(addr) + ' connected as ' + name)
        # start new thread to deal with client
        ClientListener(name, con).start()


if __name__ == "__main__":
    main()
