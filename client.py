import os
import socket
import sys

# connect
if len(sys.argv) != 4:
    print('error, expected usage: python3 client.py filename domainname|ip port')
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect((sys.argv[2], int(sys.argv[3])))

text_file = sys.argv[1]

# Send file
size = os.path.getsize(text_file)
iter = 0

with open(text_file, 'rb') as fs:
    sock.send(text_file.encode('utf-8'))
    # get ack
    sock.recv(1024).decode('utf-8')
    byte = fs.read(1024)
    transmitted_size = 1024
    while byte:
        if iter % 10 == 0:
            print(str(int((transmitted_size / size) * 100)) + '%')
        sock.send(byte)
        byte = fs.read(1024)
        transmitted_size += 1024
        iter += 1
    print('done')
sock.close()
